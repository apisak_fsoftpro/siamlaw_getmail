<?php
//  error_reporting(E_ALL);
//  ini_set('display_error', 'on');
  header('Access-Control-Allow-Origin: *');  
  $mail_id = "";
  if(isset($_GET['mail_id'])) {
    $mail_id = $_GET['mail_id'];
  }

  $hostname = '{imap-mail.outlook.com:993/imap/ssl}INBOX';
  $username = '';
  $password = '';

  $inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Outlook: ' . imap_last_error());

  $emails = imap_search($inbox,'ALL');
  $store_email = array();

  if($_GET['type'] == 'all') {
    if($emails) {
      // $output = '';
      rsort($emails);
      $from = '';
      foreach($emails as $key => $email_number) {
        $overview = imap_fetch_overview($inbox,$email_number,0);
        $headerInfo = imap_headerInfo($inbox, $overview[0]->msgno);
        $from = $headerInfo->from[0]->mailbox . "@" . $headerInfo->from[0]->host;
        // $message = imap_fetchbody($inbox,$email_number,2);
  
        // $output.= '<div class="body">'.$message.'</div>';
  
        $store_email[] = array(
          'mail_id' =>  $email_number,
          'from'    =>  $from,
          'subject' =>  iconv_mime_decode($overview[0]->subject, 0, 'utf-8'),
          'date'    =>  $overview[0]->date,
          'seen'    =>  $overview[0]->seen
        );
  
      }
    
      header("Content-Type: application/json; charset=UTF-8");
      echo json_encode($store_email);
    }

    imap_close($inbox);
  }

  if($_GET['type'] == 'single') {
    if($_GET['mail_id'] != "") {
      $message = imap_fetchbody($inbox, $_GET['mail_id'], 2);
  
      header("Content-Type: text/html; charset=UTF-8");
      echo base64_decode($message);
    }
  }

 if($_GET['type'] == 'flagRead') {
   if($_GET['mail_id'] != "") {
    $result = array();

    $status = imap_setflag_full($inbox, $_GET['mail_id'], "\\Seen");

    if($status == true) {
     $result['status'] = 'success';
    } else {
     $result['status'] = 'failed';
    }

    header("Content-Type: application/json");
    echo json_encode($result);
   }
 }

?>
